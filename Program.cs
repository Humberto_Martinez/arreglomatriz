﻿int COL_MAX = 5, ROW_MAX = 5;
int[][] MATRIZ = new int[ROW_MAX][];

MATRIZ[0] = [12, 6, 9, 4, 26];
MATRIZ[1] = [22, 16, 11, 4, 2];
MATRIZ[2] = [56, 890, 23, 4, 6];
MATRIZ[3] = [2, 12, 91, 4, 11];
MATRIZ[4] = [1, 16, 19, -4, 99];

Console.WriteLine();
//Ejercicio 1
Console.WriteLine("0.- TABLA DE CONTENIDO");

for (int row = 0; row < ROW_MAX; row++)
{
    for (int col = 0; col < COL_MAX; col++)
    {
        Console.Write("" + MATRIZ[row][col] + ", ");
    }
    Console.WriteLine();
}

Console.WriteLine();
//Ejercicio 2
Console.WriteLine("1.- TABLA DE CONTENIDO FORMATO");

for (int row = 0; row < ROW_MAX; row++)
{
    for (int col = 0; col < COL_MAX; col++)
    {
        Console.Write("|" + MATRIZ[row][col] + "| ");
    }
    Console.WriteLine();
}

Console.WriteLine();
//Ejercicio 3
Console.WriteLine("2.- TABLA DE POSICIONES");

for (int i = 0; i < ROW_MAX; i++)
{
    Console.Write("COL" + i + " " + "\t");
}
Console.WriteLine();

for (int j = 0; j < ROW_MAX; j++)
{
    for (int i = 0; i < ROW_MAX; i++)
    {
        Console.Write("[" + j + "," + i + "]\t");
    }
    Console.WriteLine();
}

Console.WriteLine();
//Ejercicio 4
Console.WriteLine("3.- DIAGONAL INVERTIDA");

for (int i = 0; i < ROW_MAX; i++)
{
    for (int j = 0; j < COL_MAX; j++)
    {
        if (i == j)
        {
            Console.Write("[" + i + "," + j + "]\t", MATRIZ[i][j]);
        }
        else
        {
            Console.Write("    ");
        }
    }
    Console.WriteLine();
}


Console.WriteLine();
//Ejercicio 5
Console.WriteLine("4.- Diagonal / ");

for (int i = 0; i < ROW_MAX; i++)
{
    for (int j = 0; j < COL_MAX; j++)
    {
        if (j + i == COL_MAX - 1)
        {
            Console.Write("[" + i + "," + j + "]\t", MATRIZ[i][j]);
        }
        else
        {
            Console.Write("    ");
        }
    }
    Console.WriteLine();
}
Console.WriteLine();

Console.WriteLine();
//Ejercicio 6
Console.WriteLine("5.- COLUMNA 2");
for (int i = 0; i < COL_MAX; i++)
{
    for (int j = 0; j < ROW_MAX; j++)
    {
        if (j == 2)
        {
            Console.Write("[" + i + "," + j + "]\t");
        }
    }
    Console.WriteLine();
}
Console.WriteLine();